import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {
    @Test
    public void adicionarContatoEPesquisalo() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        String resultado = agenda.consultar("Bernardo");
        assertEquals("555555", resultado);
    }

    @Test
    public void adicionarContatoEPesquisaloPeloTelefone() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        String resultado = agenda.consultarContato("555555");
        assertEquals("Bernardo", resultado);
    }

    @Test
    public void adicionarDoisContatosComMesmoTelefoneEPesquisar() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Mithrandir", "555555");
        agenda.adicionar("Bernardo", "555555");
        String resultado = agenda.consultarContato("555555");
        assertEquals("Mithrandir", resultado);
    }

    @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");
        String separador = System.lineSeparator();
        String esperado = String.format("Bernardo,555555%sMithrandir,444444%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}