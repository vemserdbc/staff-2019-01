import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemSempreExistenteTest {
    @Test
    public void itemSempreExistenteNaoPodeSerZerado() {
        Item item = new ItemSempreExistente(1, "Anduril");
        item.setQuantidade(0);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistenteNaoPodeSerNegativo() {
        Item item = new ItemSempreExistente(1, "Anduril");
        item.setQuantidade(-10);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistentePodeReceberOutraQuantidade() {
        Item item = new ItemSempreExistente(2, "Escudo de madeira");
        item.setQuantidade(1);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistenteNaoPodeSerCriadoComZero() {
        Item item = new ItemSempreExistente(0, "Escudo de madeira");
        assertEquals(1, item.getQuantidade());
    }
}
