import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios'

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio
    } )
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    } )
  }

  render() {
    const { episodio } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <h2>{ episodio.nome }</h2>
          <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
          <span>{ episodio.assistido ? 'sim' : 'não' }</span>
          <button onClick={ this.sortear.bind( this ) }>Próximo</button>
          <button onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button>
        </header>
      </div>
    );
  }
}

export default App;
