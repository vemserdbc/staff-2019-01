import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios'
import EpisodioUi from './components/episodioUi'
import MensagemFlash from './components/shared/mensagemFlash'
import MeuInputNumero from './components/shared/meuInputNumero'

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio, deveExibirMensagem: false
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
  }

  exibeMensagem = ( { cor, mensagem } ) => {
    this.setState( {
      cor, mensagem, deveExibirMensagem: true
    } )
  } 

  registrarNota = ( { valor, erro } ) => {
    if ( erro ) {
      // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
      // para não rodar o código como se estivesse válida a obrigatoriedade
      return this.setState( {
        deveExibirErro: erro
      } )
    } else {
      this.setState( {
        deveExibirErro: erro
      } )
    }
    
    const { episodio } = this.state
    let cor, mensagem
    if ( episodio.validarNota( valor ) ) {
      episodio.avaliar( valor )
      // TODO: centralizar mensagens num arquivo
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    } else {
      // TODO: centralizar mensagens num arquivo
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida (entre 1 e 5)'
    }
    this.exibeMensagem( { cor, mensagem } )
    
  }

  atualizarMensagem = devoExibir => {
    this.setState( {
      deveExibirMensagem: devoExibir
    } )
  }

  render() {
    const { episodio, deveExibirMensagem, mensagem, cor, deveExibirErro } = this.state

    return (
      <div className="App">
        <MensagemFlash atualizarMensagem={ this.atualizarMensagem } cor={ cor } deveExibirMensagem={ deveExibirMensagem } mensagem={ mensagem } segundos={ 5 } />
        <header className="App-header">
          <EpisodioUi episodio={ episodio } />
          <MeuInputNumero placeholder="1 a 5"
            mensagemCampo="Qual sua nota para este episódio?"
            obrigatorio={ true }
            atualizarValor={ this.registrarNota }
            deveExibirErro={ deveExibirErro }
            visivel={ episodio.assistido }/>
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear }>Próximo</button>
            <button className="btn azul" onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
