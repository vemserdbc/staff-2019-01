import React from 'react'
import PropTypes from 'prop-types'

const MeuBotao = ( { cor, texto, quandoClicar } ) =>
  <React.Fragment>
    <button className={ `btn ${ cor }` } onClick={ quandoClicar }>{ texto }</button>
  </React.Fragment>

MeuBotao.propTypes = {
  texto: PropTypes.string.isRequired,
  quandoClicar: PropTypes.func.isRequired,
  cor: PropTypes.oneOf( [ 'verde', 'azul' ] ),
}

MeuBotao.defaultProps = {
  cor: 'verde'
}

export default MeuBotao
