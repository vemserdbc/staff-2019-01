import React from 'react'
import PropTypes from 'prop-types'

const CampoDeBusca = ( { atualizarValor, placeholder } ) =>
  <React.Fragment>
    <input type="text" onBlur={ atualizarValor } placeholder={ placeholder } />
  </React.Fragment>

CampoDeBusca.propTypes = {
  atualizarValor: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
}

export default CampoDeBusca
