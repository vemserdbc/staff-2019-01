/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos;

import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.service.ClienteService;

/**
 *
 * @author tiago
 */
public class Main {
    public static void main(String[] args) {
        ClienteDTO dto = new ClienteDTO();
        dto.setNomeCliente("Cliente 1");
        dto.setCpfCliente("123");
        EnderecoDTO eDTO = new EnderecoDTO();
        eDTO.setLogradouroEndereco("Rua 1");
        eDTO.setNumeroEndereco(1);
        eDTO.setBairroEndereco("Bairro 1");
        eDTO.setCidadeEndereco("Cidade 1");
        dto.setEnderecoDTO(eDTO);
        
        ClienteService instance = new ClienteService();
        instance.salvarCliente(dto);
    }
}
