/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.Endereco;
import br.com.dbc.jogos.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of salvarCliente method, of class ClienteService.
     */
    @Test
    public void testSalvarCliente() {
        System.out.println("salvarCliente");
        ClienteDTO dto = new ClienteDTO();
        dto.setNomeCliente("Cliente 1");
        dto.setCpfCliente("123");
        EnderecoDTO eDTO = new EnderecoDTO();
        eDTO.setLogradouroEndereco("Rua 1");
        eDTO.setNumeroEndereco(1);
        eDTO.setBairroEndereco("Bairro 1");
        eDTO.setCidadeEndereco("Cidade 1");
        dto.setEnderecoDTO(eDTO);
        
        ClienteService instance = new ClienteService();
        instance.salvarCliente(dto);
        Session session = HibernateUtil.getSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        List<Endereco> enderecos = session.createCriteria(Endereco.class).list();
        
        Assert.assertEquals("Erro na contagem de clientes", 1, clientes.size());
        Assert.assertEquals("Erro no nome do Cliente", "Cliente 1", clientes.get(0).getNome());
        Assert.assertEquals("Erro no cpf do Cliente", "123", clientes.get(0).getCpf());
        
        Assert.assertEquals("Erro na contagem de enderecos", 1, enderecos.size());
        Assert.assertEquals("Erro no logradouro do enderecos", "Rua 1", enderecos.get(0).getLogradouro());
        Assert.assertEquals("Erro no numero do enderecos", Integer.valueOf("1"), enderecos.get(0).getNumero());
        Assert.assertEquals("Erro no bairro do enderecos", "Bairro 1", enderecos.get(0).getBairro());
        Assert.assertEquals("Erro no cidade do enderecos", "Cidade 1", enderecos.get(0).getCidade());
        
    }
    
}
