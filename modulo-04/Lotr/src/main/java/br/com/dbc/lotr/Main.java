package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Gustavo");
        dto.setNomeUsuario("Gustavo");
        dto.setCpfUsuario(Long.valueOf("123"));
        dto.setSenhaUsuario("123");
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua do Gustavo");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("Bairro do Gustavo");
        enderecoDTO.setCidade("Cidade do Gustavo");
        enderecoDTO.setComplemento("CEP 123");
        dto.setEndereco(enderecoDTO);
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Gustavo");
        personagemDTO.setDanoElfo(123d);
        dto.setPersonagem(personagemDTO);
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    }
    
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            Usuario usuario = new Usuario();
            usuario.setNome("Antônio");
            usuario.setApelido("Antônio");
            usuario.setCpf(123l);
            usuario.setSenha("456");

            Endereco endereco = new Endereco();
            endereco.setLogradouro("Rua do Antônio");
            endereco.setNumero(100);
            endereco.setBairro("Bairro do Antônio");
            endereco.setComplemento("Apartamento do Antônio");
            endereco.setCidade("Cidade do Antônio");
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("Rua do Antônio");
            endereco2.setNumero(150);
            endereco2.setBairro("Bairro do Antônio");
            endereco2.setComplemento("Apartamento do Antônio");
            endereco2.setCidade("Cidade do Antônio");

            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("Celular");
            tipoContato.setQuantidade(10);

            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("51999886677");

            tipoContato.setContato(contato);
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);

            session.save(usuario);

            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao);

            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo Bonceiro");
            session.save(hobbitTabelao);

            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);

            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Bilbo");
            hobbitPerClass.setDanoHobbit(1d);
            session.save(hobbitPerClass);

            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Joinha");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);

            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Smigle");
            hobbitJoin.setDanoHobbit(1d);
            session.save(hobbitJoin);

            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antônio"),
                            Restrictions.ilike("endereco.cidade", "%do antônio")
                    ));

            List<Usuario> usuarios = criteria.list();

            System.out.println(usuarios);
            usuarios.forEach(System.out::println);
            
            //conta quantidade de linhas
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antônio"),
                            Restrictions.ilike("endereco.cidade", "%do antônio")
                    ));
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            //conta quantidade de enderecos
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antônio"),
                            Restrictions.ilike("endereco.cidade", "%do antônio")
                    ));

            criteria.setProjection(Projections.count("enderecos"));
            System.out
                    .println(String
                            .format("Foram encontrados %s endereco(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            
            
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antônio"),
                            Restrictions.ilike("endereco.cidade", "%do antônio")
                    ));

            criteria.setProjection(Projections.sum("endereco.numero"));
            System.out
                    .println(String
                            .format("Foram somados números de endereco(s)"
                                    + " e deu %s "
                                    + "para os critérios especificados",
                                    criteria.uniqueResult()));
            
            usuarios = session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ")
                    .list();
            
            usuarios.forEach(System.out::println);
            
            Long count = (Long)session
                    .createQuery("select count(distinct u.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s usuarios", count));
            
            count = (Long)session
                    .createQuery("select count(endereco.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s enderecos", count));
            
            Long sum = (Long)session
                    .createQuery("select sum(endereco.numero) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ")
                    .uniqueResult();
            System.out.println(String.format("Somamos com HQL os números "
                    + "dos enderecos e deu %s", sum));

            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        System.exit(0);

    }

}
